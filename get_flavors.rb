require_relative 'init/libs'
require_relative 'init/globals'
require_relative 'helpers/datacenter/parser'
require_relative 'helpers/datacenter/parsehelper'
require_relative 'helpers/servicehelper'

# start service instance
initService()

response = @service.list_flavors
body = response.body
body['flavors'].each do |flavor|
  line = "id : " + flavor['id'] + ", name : " + flavor["name"]
  File.open('flavors.txt', 'a') do |fl|  
    fl.puts line
  end 
end

puts "file ./flavors.txt generated. \n\n"
