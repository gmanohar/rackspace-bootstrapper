require_relative 'init/libs'
require_relative 'init/globals'
require_relative 'helpers/datacenter/parser'
require_relative 'helpers/datacenter/parsehelper'
require_relative 'helpers/servicehelper'

# start service instance
initService()

response = @service.list_images
body = response.body
body['images'].each do |image|
  line = "id : " + image['id'] + ", name : " + image["name"]
  File.open('images.txt', 'a') do |fl|  
    fl.puts line
  end 
end

puts "file ./images.txt generated. \n\n"
