require_relative 'init/libs'
require_relative 'init/globals'
require_relative 'helpers/environment/parser'
require_relative 'helpers/environment/parsehelper'
require_relative 'helpers/servicehelper'
require_relative 'helpers/environment/nodehelper'

# declare the base directory
@basedir = Dir.pwd

# load the common parser config
commonConfig()

# start service and fire up the machines
initService()
bootstrapAllNodes()
