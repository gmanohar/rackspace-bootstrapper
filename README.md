[WIP] Manage Rackspace & Puppet environment from command line. 

## Getting Started

* Get Ruby ( Version 1.9.2 or later)    
* Clone this repo   
* Ensure you have correct values for "username","api-key" in ./init/init.json.      
* The public key name you define in ./init/init.json is registered with Rackspace.     
* The public key MUST exist on the machine these scripts are running from.    
* ` $ gem install fog `      
* ` $ gem install curb `       

## Getting Datacenter details

Get all the flavors available for a VM in a given region, for example N.Virgina (iad)     
``` 
$ ruby get_flavors.rb -r iad 
```   

Get all the images available for a VM in a given region, for example Chicago (ord)     
```
$ ruby get_images.rb -r ord
```     

Get all servers in a datacenter    
```
$ ruby get_servers.rb -r iad
``` 

Get all servers in a datacenter in given regiob & environment, for example "dit1"    
```
$ ruby get_servers.rb -r iad -e dit1
``` 
## node_bootstrap.rb

See all options    
```
$ ruby node_bootstrap.rb -h        

Usage: node_bootstrap.rb [options]         
    -d, --dns dns                    DNS server ip     
    -e, --environment environment    Puppet environment. eg: na1      
    -f, --flavor flavor              Rackspace flavor id. eg: 4       
    -g, --group group                Puppet Node Group. eg: cdina1      
    -m, --master master              Puppet master ip & fqdn. eg : 192.123.45.5,cinf-puppet-09     
    -n, --name name                  Node name. eg: ceedallas-mysql-01     
    -N, --network network            Network id & name. eg : 2343edf3-w33dfd-333-sfefe4,LEO_HK    
    -r, --region region              Rackspace Region. eg : ord     
    -i, --image image                Rackspace image id. Default is RHEL 6 PV     
    -S, --sshkey sshkey              SSH key. Default will be any given system administrators local key   
    -R, --role role                  Puppet Node role. eg: apache::dev   
    -F, --facts facts                Puppet Node custom facts. eg: datacenter=shitspace,role=cdi   
    -s, --search search              Domain to be searched/appended. eg: leo.demo    
    -h, --help                       Displays Help    
```    


Start a VM in N.Virginia(iad) RC-CLOUD-INSIDE4 with default image (RHEL 6 PV) & default ssh key. 
```
$ ruby node_bootstrap.rb -n node-test-1 -f 2 -N 72313c6d-22ad-441e-bde3-edffc0a2fb62,RC-CLOUD-INSIDE4 -r iad
```      

Start a VM in N.Virginia(iad)  RC-CLOUD-INSIDE4 with custom image & custom ssh key

```
ruby node_bootstrap.rb -n node-test-2 -f 2 -N 72313c6d-22ad-441e-bde3-edffc0a2fb62,RC-CLOUD-INSIDE4 -r iad -i 2318853e-f3b1-4cf4-b1a4-d7db71ca9b50 -S gautam-local
```

Start a VM in Chicago(ord), Install puppet and connect it to the puppet master in a given environment, with custom facts. NOTE - The puppet master must be configured to auto-sign agent certificates.    

```
ruby node_bootstrap.rb -n node-test-1 -f 2 -N e465403c-6904-4f71-9557-34fcf401f033,RC-CLOUD-INSIDE1 -r ord -m 172.24.16.39,puppet-bootstrap-test -e na1 -F datacenter=rackspace,role=cdi -s ord.eems
```

## master_bootstrap.rb     

coming up ....

## env_bootstrap.rb     

coming up .... refer to script 

## manage_puppet.rb

Get all existing groups
```

```