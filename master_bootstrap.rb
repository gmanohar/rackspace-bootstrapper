require_relative 'init/libs'
require_relative 'init/globals'
require_relative 'helpers/master/parser'
require_relative 'helpers/master/parsehelper'
require_relative 'helpers/servicehelper'
require_relative 'helpers/master/nodehelper'
require_relative 'helpers/master/boothelper'

# declare the base directory
@basedir = Dir.pwd

# load the common parser config
commonConfig()

# start service and fire up the machine
initService()
startNode()

# bootstrap the puppet server
bootstrapPuppet()
