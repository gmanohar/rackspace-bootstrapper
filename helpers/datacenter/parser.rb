@options = {
  :region => nil,
  :environment => nil,
}

parser = OptionParser.new do|opts|

  opts.banner = "\nUsage: get_flavors.rb [options]"

  opts.on('-r', '--region region', 'Rackspace Region. eg : ord') do |region|
    @options[:region] = region;
    end

  opts.on('-e', '--environment environment', 'environment string used in the node fqdn. eg : ceedit1') do |environment|
    @options[:environment] = environment;
    end
 
  opts.on('-h', '--help', 'Displays Help') do
    puts opts
    puts "\n"
    exit
    end

end

parser.parse!

