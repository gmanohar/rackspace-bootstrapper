@options = {
  :network => nil,
  :region => nil,
  :environment => nil,
  :master => nil,
  :sshkey => nil
}

parser = OptionParser.new do|opts|

  opts.banner = "\nUsage: env_bootstrap.rb [options]"
  opts.banner += "\n\n"

  opts.on('-N', '--network network', 'Network id & name. eg : 2343edf3-w33dfd-333-sfefe4,LEO_HK') do |network|
    @options[:network] = network;
    end

  opts.on('-r', '--region region', 'Rackspace Region. eg : ord') do |region|
    @options[:region] = region;
    end

  opts.on('-S', '--sshkey sshkey', 'SSH key. Default will be any given system administrators local key') do |sshkey|
    @options[:sshkey] = sshkey;
    end

  opts.on('-m', '--master master', 'Puppet master ip & fqdn. eg : 192.123.45.5,cinf-puppet-09') do |master|
    @options[:master] = master;
    end

  opts.on('-e', '--environment environment', 'Puppet environment. eg: na1') do |environment|
    @options[:environment] = environment;
    end


  opts.on('-h', '--help', 'Displays Help') do
    puts opts
    puts "\n"
    exit
    end

end

parser.parse!

