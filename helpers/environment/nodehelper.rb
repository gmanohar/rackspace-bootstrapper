# create the API service
def bootstrapAllNodes() 


  # get the network details
  networkarray = @options[:network].split(',')
  networkid = networkarray[0]
  networkname = networkarray[1]

  # get the ssh key either
  # user supplied or default
  if @options[:sshkey] != nil
    sshkey = @options[:sshkey]
  else
    sshkey = @keyname
  end

  # get the file with all machine descriptions
  machinesfile = @basedir.to_s + "/init/environment_machines.json"
  machineslist = File.read(machinesfile)
  machines = JSON.parse(machineslist)

  machines.each do |machine|

    puts "Initiating Server\n"
    puts "\n******************** THIS MAY TAKE SOME TIME ********************"

    # first start the node
    server = @service.servers.create(
      :name => machine['name'],
      :flavor_id => machine['flavor'],
      :image_id => machine['image'],
      :password => @rootpassword,
      :key_name => sshkey,
      :networks => [networkid,]
    )

    # wait for the server to provision
    server.wait_for(1200, 5) do
      print "................................."
      STDOUT.flush
      ready?
    end
    server.reload

    # get the ip
    networkarray = server.addresses 
    ip = networkarray[networkname].first
    nodeip = ip["addr"]
    puts "\n"
    puts "Created Server "+machine['name']+" with IP : "+nodeip
    puts "\n"
    
    # now bootstrap the node
    bootstrapNode(nodeip,machine['custom_facts'])

  end


end


def bootstrapNode(nodeip,facts)

    puts "####################################################################"
    puts "################ STARTING THE BOOTSTRAPPING PROCESS ################"
    puts "####################################################################"
    puts "\n"  

    # initiate the command string
    @commands = 'date'
    
    # get the main file with commands
    commandfile = @basedir.to_s + "/init/exec.txt"
    File.readlines(commandfile).each do |line|
      com = line.gsub("\n",'').squeeze(' ')
      com.strip!
      if com[0]
        @commands = @commands + " && " + com
      end 
    end

    
    # process the custom facts
    factarray = facts.split(',')
  
    # create the final command string
    finalcommand = ''
    factarray.each do |fact|
      finalcommand = finalcommand + " && echo \"#{fact}\" >> /etc/puppetlabs/facter/facts.d/eems.txt"
    end


    # replace the place holders
    # first get the master host & ip
    master = @options[:master]
    masterarray = master.split(',')
    masterip = masterarray[0]
    masterhost = masterarray[1]
    masterhoststring = masterip + ' ' + masterhost 

    # replace puppet ip/host  placeholder
    remotecom = @commands.gsub("<master_host_ip>", masterhoststring)

    # replace search  placeholder
    # remotecom = remotecom.gsub("<search_domain>", @options[:search])

    # replace puppet host placeholder
    remotecom = remotecom.gsub("<master_host>", masterhost)

    # replace environment placeholder
    remotecom = remotecom.gsub("<env>", @options[:environment])
    remotecom += finalcommand

    # prepare the commands for rake 
    execute = <<BOOTSTRAP 
#{remotecom} 
BOOTSTRAP

    # ssh details
    ssh = 'ssh -t -o StrictHostKeyChecking=no root@' + nodeip
    
    sh "#{ssh} '#{execute}'"
    puts "\n"
    puts "####################################################################"
    puts "################# COMPLETED BOOTSTRAPPING PROCESS ##################"
    puts "####################################################################"
    puts "\n"  

end
