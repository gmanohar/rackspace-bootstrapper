def commonConfig()

  while @options[:network] == nil ||@options[:network] == ''
    print 'Enter node network id: '
        @options[:network] = gets.chomp
  end

  while @options[:region] == nil ||@options[:region] == ''
    print 'Enter Datacenter region [ord/iad/dfw]: '
        @options[:region] = gets.chomp
  end

  while @options[:master] == ''
    print 'Enter Puppet Master name & ip: '
    @options[:master] = gets.chomp
  end

  while @options[:environment] == nil || @options[:environment] == ''
    print 'Enter node environment: '
    @options[:environment] = gets.chomp
  end

end

