def nodeConfig()

  # node name
  while @options[:name] == nil ||@options[:name] == ''
    print 'Enter node name : '
        @options[:name] = gets.chomp
  end

  # node flavor
  while @options[:flavor] == nil ||@options[:flavor] == ''
    print 'Enter node flavor (rackspace flavor id) : '
        @options[:flavor] = gets.chomp
  end

  # node region
  while @options[:region] == nil ||@options[:region] == ''
    print 'Enter Datacenter region [ord/iad/dfw]: '
        @options[:region] = gets.chomp
  end

  # node network
  while @options[:network] == nil ||@options[:network] == ''
    print 'Enter node network id & name: '
        @options[:network] = gets.chomp
  end


  # if node has to be attached to master
  if @options[:master] != nil

    # node master
    while @options[:master] == ''
      print 'Enter Puppet Master name & ip: '
          @options[:master] = gets.chomp
    end

    # node environment
    while @options[:environment] == nil || @options[:environment] == ''
      print 'Enter node environment: '
        @options[:environment] = gets.chomp
    end
    
    # node custom facts
    while @options[:facts] == nil ||@options[:facts] == ''
      print 'Enter node custom facts : '
        @options[:facts] = gets.chomp
    end


    #this may be removed
    while @options[:search] == nil ||@options[:search] == ''
      print 'Enter Domains to be searched/appended: '
        @options[:search] = gets.chomp
    end

  end # END OF MASTER CONDITION CHECK



  #while @options[:group] == nil ||@options[:group] == ''
  #  print 'Enter node group : '
  #      @options[:group] = gets.chomp
  #end

  #while @options[:role] == nil ||@options[:role] == ''
  #  print 'Enter node role : '
  #      @options[:role] = gets.chomp
  #end



  #while @options[:dns] == nil || @options[:dns] == ''
  #  print 'Enter DNS Server ip: '
  #    @options[:dns] = gets.chomp
  #end



end # END OF NODE CONFIG  FUNCTION




# not used at the moment
def withConfig()

  readfile = @options[:config]
  if !File.file?(readfile)
    puts "\nERROR : Config file ["+readfile+"] not found \n\n"
    exit
  end
  
  configfile = File.read(readfile)
  config_hash = JSON.parse(configfile)
  
  @options[:region] = config_hash["region"];
  @options[:network] = config_hash["network_id"];
  @options[:network_name] = config_hash["network_name"];
  @options[:master] = config_hash["master"];
  @options[:environment] = config_hash["environment"];
  @options[:dns] = config_hash["dns"];

end
