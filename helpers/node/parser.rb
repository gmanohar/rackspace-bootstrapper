@options = {
  :config => nil,
  :dns => nil,
  :environment => nil,
  :flavor => nil,
  :group => nil,
  :master => nil,
  :name => nil,
  :network => nil,
  :networkid => nil,
  :networkname => nil,
  :region => nil,
  :image => nil,
  :sshkey => nil,
  :role => nil,
  :facts => nil,
  :search => nil
}

parser = OptionParser.new do|opts|

  opts.banner = "\nUsage: node_bootstrap.rb [options]"
  opts.banner += "\n\n"

  #opts.on('-c', '--config config', 'Config file (/provide/absolute/path)') do |config|
  #  @options[:config] = config;
  #  end

  opts.on('-d', '--dns dns', 'DNS server ip') do |dns|
    @options[:dns] = dns;
    end

  opts.on('-e', '--environment environment', 'Puppet environment. eg: na1') do |environment|
    @options[:environment] = environment;
    end

  opts.on('-f', '--flavor flavor', 'Rackspace flavor id. eg: 4') do |flavor|
    @options[:flavor] = flavor;
    end

  opts.on('-g', '--group group', 'Puppet Node Group. eg: cdina1') do |group|
    @options[:group] = group;
    end

  opts.on('-m', '--master master', 'Puppet master ip & fqdn. eg : 192.123.45.5,cinf-puppet-09') do |master|
    @options[:master] = master;
    end

  opts.on('-n', '--name name', 'Node name. eg: ceedallas-mysql-01') do |name|
    @options[:name] = name;
    end

  opts.on('-N', '--network network', 'Network id & name. eg : 2343edf3-w33dfd-333-sfefe4,LEO_HK') do |network|
    @options[:network] = network;
    end

  opts.on('-r', '--region region', 'Rackspace Region. eg : ord') do |region|
    @options[:region] = region;
    end

  opts.on('-i', '--image image', 'Rackspace image id. Default is RHEL 6 PV') do |image|
    @options[:image] = image;
    end

  opts.on('-S', '--sshkey sshkey', 'SSH key. Default will be any given system administrators local key') do |sshkey|
    @options[:sshkey] = sshkey;
    end
 
  opts.on('-R', '--role role', 'Puppet Node role. eg: apache::dev') do |role|
    @options[:role] = role;
    end

  opts.on('-F', '--facts facts', 'Puppet Node custom facts. eg: datacenter=shitspace,role=cdi') do |facts|
    @options[:facts] = facts;
    end
  
  #this may be removed 
  opts.on('-s', '--search search', 'Domain to be searched/appended. eg: leo.demo') do |search|
    @options[:search] = search;
    end
 
  opts.on('-h', '--help', 'Displays Help') do
    puts opts
    puts "\n"
    exit
    end

end

parser.parse!

