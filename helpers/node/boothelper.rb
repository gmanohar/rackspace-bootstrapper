def bootstrapNode()


  puts "####################################################################"
  puts "################ STARTING THE BOOTSTRAPPING PROCESS ################"
  puts "####################################################################"
  puts "\n"  


  # initiate the command string
  @commands = 'date'

  # get the main file with commands
  commandfile = @basedir.to_s + "/init/exec.txt"

  File.readlines(commandfile).each do |line|
    com = line.gsub("\n",'').squeeze(' ')
    com.strip!
    if com[0]
      @commands = @commands + " && " + com
    end 
  end

  # process the custom facts
  facts = @options[:facts]
  factarray = facts.split(',')
  
  # create the command string
  finalcommand = ''
  factarray.each do |fact|
    finalcommand = finalcommand + " && echo \"#{fact}\" >> /etc/puppetlabs/facter/facts.d/eems.txt"
  end

  # replace the place holders
  # first get the master host & ip
  master = @options[:master]
  masterarray = master.split(',')
  masterip = masterarray[0]
  masterhost = masterarray[1]
  masterhoststring = masterip + ' ' + masterhost 

  # replace puppet ip/host  placeholder
  remotecom = @commands.gsub("<master_host_ip>", masterhoststring)

  # replace search  placeholder
  # remotecom = remotecom.gsub("<search_domain>", @options[:search])

  # replace puppet host placeholder
  remotecom = remotecom.gsub("<master_host>", masterhost)

  # replace environment placeholder
  remotecom = remotecom.gsub("<env>", @options[:environment])
  remotecom += finalcommand

  # prepare the commands for rake 
  execute = <<BOOTSTRAP 
#{remotecom} 
BOOTSTRAP

  # ssh details
  ssh = 'ssh -t -o StrictHostKeyChecking=no root@' + @nodeip
  
  sh "#{ssh} '#{execute}'"
  puts "\n"
  puts "####################################################################"
  puts "################# COMPLETED BOOTSTRAPPING PROCESS ##################"
  puts "####################################################################"
  puts "\n"  


end
