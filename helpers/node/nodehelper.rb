# create the API service
def startNode() 

  puts "Initiating Server\n"
  puts "\n******************** THIS MAY TAKE SOME TIME ********************"

  # get the network details
  networkarray = @options[:network].split(',')
  networkid = networkarray[0]
  networkname = networkarray[1]

  # get the image  either
  # user supplied or default
  if @options[:image] != nil
    image = @options[:image]
  else
    image = @image
  end  

  # get the ssh key either
  # user supplied or default
  if @options[:sshkey] != nil
    sshkey = @options[:sshkey]
  else
    sshkey = @keyname
  end

  @server = @service.servers.create(
    :name => @options[:name],
    :flavor_id => @options[:flavor],
    :image_id => image,
    :password => @rootpassword,
    :key_name => sshkey,
    :networks => [networkid,]
  )

  @server.wait_for(600, 5) do
    print "................................."
    STDOUT.flush
    ready?
  end
  @server.reload
  networkarray = @server.addresses 
  ip = networkarray[networkname].first
  @nodeip = ip["addr"]
  puts "\n"
  puts "Created Server with IP : "+@nodeip
  puts "\n"

end
