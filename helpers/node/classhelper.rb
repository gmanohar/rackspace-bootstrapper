
def classifyNode() 

  # prepare the payload
  jsonstring = '{"rule":["and",["~",["fact","fqdn"],"' + @options[:name] + '"]],'
  jsonstring += '"environment_trumps":true,"name":"' + @options[:group] + '",'
  jsonstring += '"parent":"00000000-0000-4000-8000-000000000000",'
  jsonstring += '"environment":"' + @options[:environment]  + '","classes":{"' + @options[:role] + '":{}}}'

  c = Curl::Easy.new("https://puppet-bootstrap-test:4433/classifier-api/v1/groups")
  c.headers["Content-Type"] = "application/json"
  c.verbose = true 
  c.cert = '/home/gautam/web/ebay/production/rackspace_bootstrapper/keys/certs/puppet-bootstrap-test.pem'
  c.cert_key = '/home/gautam/web/ebay/production/rackspace_bootstrapper/keys/private_keys/puppet-bootstrap-test.pem'
  c.cacert = '/home/gautam/web/ebay/production/rackspace_bootstrapper/keys/ca.pem'
  c.post(jsonstring)
  c.close

end
