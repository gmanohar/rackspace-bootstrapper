def bootstrapPuppet()


  puts "####################################################################"
  puts "################ STARTING THE BOOTSTRAPPING PROCESS ################"
  puts "####################################################################"
  puts "\n"  

  # prepare the answers file
  answerfile = @basedir.to_s + "/templates/puppet.answers"

  # get the main file with commands
  commandfile = @basedir.to_s + "/init/puppet-exec.txt"

  # initiate the command string
  @commands = 'cd /tmp'

  # first read the answer file
  File.readlines(answerfile).each do |answer|
    ans = answer.gsub("\n",'').squeeze(' ')
    ans.strip!
    if ans[0]
      @commands = @commands + " && echo \"#{ans}\" >> /tmp/puppet.answers"
    end 
  end

  # next read the main commands
  File.readlines(commandfile).each do |line|
    com = line.gsub("\n",'').squeeze(' ')
    com.strip!
    if com[0]
      @commands = @commands + " && " + com
    end 
  end

  # replace the master fqdn place holder
  remotecom = @commands.gsub("<master-name>", @options[:name])

  # prepare the commands for rake 
  execute = <<BOOTSTRAP 
#{remotecom} 
BOOTSTRAP

  # ssh details
  ssh = 'ssh -t -o StrictHostKeyChecking=no root@' + @nodeip
  
  sh "#{ssh} '#{execute}'"
  puts "\n"
  puts "####################################################################"
  puts "################# COMPLETED BOOTSTRAPPING PROCESS ##################"
  puts "####################################################################"
  puts "\n"  


end
