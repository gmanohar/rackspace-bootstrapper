def commonConfig()

  while @options[:flavor] == nil ||@options[:flavor] == ''
    print 'Enter node flavor (rackspace flavor id) : '
        @options[:flavor] = gets.chomp
  end

  while @options[:name] == nil ||@options[:name] == ''
    print 'Enter node name : '
        @options[:name] = gets.chomp
  end

  while @options[:network] == nil ||@options[:network] == ''
    print 'Enter node network id: '
        @options[:network] = gets.chomp
  end

  while @options[:region] == nil ||@options[:region] == ''
    print 'Enter Datacenter region [ord/iad/dfw]: '
        @options[:region] = gets.chomp
  end


end

