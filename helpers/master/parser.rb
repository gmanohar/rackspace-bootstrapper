@options = {
  :flavor => nil,
  :image => nil,
  :name => nil,
  :network => nil,
  :region => nil,
  :sshkey => nil
}

parser = OptionParser.new do|opts|

  opts.banner = "\nUsage: master_bootstrap.rb [options]"
  opts.banner += "\n\n"

  opts.on('-f', '--flavor flavor', 'Rackspace flavor id. eg: 4') do |flavor|
    @options[:flavor] = flavor;
    end

  opts.on('-i', '--image image', 'Rackspace image id. Default is RHEL 6 PV') do |image|
    @options[:image] = image;
    end

  opts.on('-n', '--name name', 'Node name. eg: ceeinf-puppet-01') do |name|
    @options[:name] = name;
    end

  opts.on('-N', '--network network', 'Network id & name. eg : 2343edf3-w33dfd-333-sfefe4,LEO_HK') do |network|
    @options[:network] = network;
    end

  opts.on('-r', '--region region', 'Rackspace Region. eg : ord') do |region|
    @options[:region] = region;
    end

  opts.on('-S', '--sshkey sshkey', 'SSH key. Default will be any given system administrators local key') do |sshkey|
    @options[:sshkey] = sshkey;
    end

  opts.on('-h', '--help', 'Displays Help') do
    puts opts
    puts "\n"
    exit
    end

end

parser.parse!

