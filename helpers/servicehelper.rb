# create the API service
def initService() 
  
  puts "\nCreating Rackspace Service Instance"
  @service = Fog::Compute.new(
    :provider => @provider,
    :rackspace_username => @username,
    :rackspace_api_key => @apikey,
    :rackspace_region => @options[:region]
  )

end
