# read the globals filedata
globalsfile = File.read('init/init.json')
globals_hash = JSON.parse(globalsfile)

# globals
@image =  globals_hash['image']
@rootpassword = globals_hash['rootpassword']
@keyname = globals_hash['keyname']
@apikey = globals_hash['apikey']
@username = globals_hash['username']
@provider = globals_hash['provider']
