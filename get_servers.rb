require_relative 'init/libs'
require_relative 'init/globals'
require_relative 'helpers/datacenter/parser'
require_relative 'helpers/datacenter/parsehelper'
require_relative 'helpers/servicehelper'

# start service instance
initService()

response = @service.list_servers
body = response.body

body['servers'].each do |server|

  if @options[:environment] != nil && @options[:environment] != ''
    if server['name'].include? @options[:environment]
      line = server['name'] + "|" + server["addresses"].to_s
      File.open('servers.txt', 'a') do |fl|  
        fl.puts line
      end 
    end
  else
    line = server['name'] + "|" + server["addresses"].to_s
    File.open('servers.txt', 'a') do |fl|  
      fl.puts line
    end 
  end
  

end

puts "file ./servers.txt generated. \n\n"
