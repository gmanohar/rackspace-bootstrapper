require_relative 'init/libs'
require_relative 'init/globals'
require_relative 'helpers/node/parser'
require_relative 'helpers/node/parsehelper'
require_relative 'helpers/servicehelper'
require_relative 'helpers/node/nodehelper'
require_relative 'helpers/node/boothelper'
require_relative 'helpers/node/classhelper'

# declare the base directory
@basedir = Dir.pwd

# load node config
nodeConfig()

# now that all params are available
# fire up the machine on Rackspace
initService()
startNode()

# if user has provided a puppet master
# install puppet on the node and
# execute all custom commands
if @options[:master] != nil && @options[:environment] != nil && @options[:search] != nil
  bootstrapNode()
end

# If user has provided all role,
# group information classify node
if @options[:role] != nil && @options[:group] != nil
  classifyNode()
end
